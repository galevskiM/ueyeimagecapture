//
// Created by marko on 30/04/18.
//

#pragma once
// Adapted from uEyeImageQueue Example by IDS
//

#include "uEye.h"
#include <vector>

// NOTE: uEye.h defines INT, BOOL, HANDLE, DWORD, and others as int32_t.
class uEyeUSB3
{
public:
    ~uEyeUSB3();

    //camera variables
    HIDS    my_hcamera;     //  CameraHandle
    int32_t     my_nColorMode;  //  Y8/RGB16/RGB24/REG32
    int32_t     my_BitsPerPixel;//  bits per pixel
    int32_t     my_nSizeX;      //  number of columns
    int32_t     my_nSizeY;      //  number of rows
    SENSORINFO my_sInfo;   //  sensor info struct
    IMAGE_FILE_PARAMS imageFileParams;




    //memory + sequence buffers
    std::vector<int32_t> my_videoSeqMemId;      // camera memory - buffer IDs
    std::vector<char*> my_vpcSeqImgMem;     // camera memory - pointers to buffer
    uint8_t my_nNumberOfBuffers;                // number of buffers used.

    //img acqui. thread variables

    HANDLE  my_hThreadImageQueue;
    BOOL    my_boRunThreadImageQueue;
    BOOL    my_boTerminatedThreadImageQueue;
    DWORD   my_dwThreadIDImageQueue;

    //camera functions

    INT InitCamera(HIDS * hcamera);

    bool Open();
    bool Close();
    bool GrabImagesFromQueue(void);
    bool CamSeqBuild();
    bool CamSeqKill();

    void ThreadProcImageQueue();

};
void * threadProcImageQueueInit(void * pv);
