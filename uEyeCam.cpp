#include "uEyeCam.h"
#include "uEye.h"
#include <stdio.h>
#include <pthread.h>
#include <string>
#include <sstream>
#include <cstring>
#include <iomanip>
#include <fstream>

pthread_t pthread1;
pthread_attr_t attr;

/**
 * Initialises the camera and downloads new firmware if necessary.
 * @param hcamera - the camera's handle
 * @return returnVal - int32_t typedef for success
 */
INT uEyeCam::InitCamera(HIDS * hcamera) {
    INT returnVal = is_InitCamera(hcamera, nullptr);

    if (returnVal == IS_STARTER_FW_UPLOAD_NEEDED) {
        INT nUploadTime = 25;
        is_GetDuration(*hcamera, IS_STARTER_FW_UPLOAD, &nUploadTime);

        printf("The camera requires new firmware. The upload will take about %d seconds. Please wait...\n", nUploadTime);

        *hcamera = (HIDS) (((INT) *hcamera) | IS_ALLOW_STARTER_FW_UPLOAD);
        returnVal = is_InitCamera(hcamera, NULL);
    }

    return returnVal;
}

uEyeCam::~uEyeCam() {}

/**
 * Opens the camera, calling the init function, setting camera mode, and allocating a block of image memory
 * @return returns success/failure
 */
bool uEyeCam::CamOpen(){

    INT returnVal;

    INT iImgMemID = 0;
    char* pcImgMem = nullptr;

    if (my_hcamera != 0 || my_hcamera != 1)
        returnVal = is_ExitCamera(my_hcamera);

    my_hcamera = (HIDS) 0;
    returnVal = InitCamera(&my_hcamera);

    if (returnVal == IS_SUCCESS) {
        is_GetSensorInfo(my_hcamera, &my_sInfo);

        returnVal = is_SetErrorReport(my_hcamera, IS_DISABLE_ERR_REP);
        if (returnVal != IS_SUCCESS)
        {
            printf("ERROR: Cannot disable automatic error reports \n");
            return false;
        }
        SENSORINFO SensorInfo;
        is_GetSensorInfo(my_hcamera, &SensorInfo);
        CAMINFO CamInfo;
        is_GetCameraInfo(my_hcamera, &CamInfo);

        //assuming colour camera:
        my_BitsPerPixel = 24;
        my_nColorMode = IS_CM_BGR8_PACKED;

        is_SetColorMode(my_hcamera, my_nColorMode);

        IS_SIZE_2D imageSize;
        is_AOI(my_hcamera, IS_AOI_IMAGE_GET_SIZE, (void*)&imageSize, sizeof(imageSize));

        my_nSizeX = imageSize.s32Width;
        my_nSizeY = imageSize.s32Height;
        is_AOI(my_hcamera, IS_AOI_IMAGE_GET_POS_X_ABS, (void*)&nAbsPosX, sizeof(nAbsPosX));
        is_AOI(my_hcamera, IS_AOI_IMAGE_GET_POS_Y_ABS, (void*)&nAbsPosY, sizeof(nAbsPosY));

        returnVal = is_AllocImageMem(my_hcamera, my_nSizeX, my_nSizeY, my_BitsPerPixel, &pcImgMem, &iImgMemID);

        if (returnVal == IS_SUCCESS){

            is_SetImageMem(my_hcamera, pcImgMem, iImgMemID);
        }
    }

    else {
        printf("ERROR: Cannot open the uEye Camera!\n");
        return false;
    }
    return true;
}

bool uEyeCam::SetEnableAutoWhiteBalance(BOOL OnOff) {
    INT returnVal;
    double param1 = OnOff;

    returnVal = is_SetAutoParameter(my_hcamera, IS_SET_ENABLE_AUTO_WHITEBALANCE, &param1, nullptr);

    return (returnVal == IS_SUCCESS);
}

double uEyeCam::GetEnableAutoWhiteBalance(){
    double param1;
    INT returnVal = is_SetAutoParameter(my_hcamera, IS_GET_ENABLE_AUTO_WHITEBALANCE, &param1, nullptr);
    if (returnVal == IS_SUCCESS) return param1;

    return NULL;
}

bool uEyeCam::SetAutoWhiteBalanceIterative(BOOL OnOff){
    double param1 = OnOff;
    INT returnVal = is_SetAutoParameter(my_hcamera, IS_SET_AUTO_WB_ONCE, &param1, nullptr);
    return (returnVal == IS_SUCCESS);
}

double uEyeCam::GetAutoWhiteBalanceIterative(){
    double param1;
    INT returnVal = is_SetAutoParameter(my_hcamera, IS_GET_AUTO_WB_ONCE, &param1, nullptr);
    if (returnVal == IS_SUCCESS) return param1;

    return NULL;
}

bool uEyeCam::SetAutoExposureEnable(BOOL OnOff) {
    INT returnVal;
    double param1 = OnOff;

    returnVal = is_SetAutoParameter(my_hcamera, IS_SET_ENABLE_AUTO_SHUTTER, &param1, nullptr);
    return (returnVal == IS_SUCCESS);
}

double uEyeCam::GetAutoExposureEnable(){
    INT returnVal;
    double param1;
    returnVal = is_SetAutoParameter(my_hcamera, IS_GET_ENABLE_AUTO_SHUTTER, &param1, nullptr);
    if (returnVal == IS_SUCCESS) return param1;

    return NULL;
}

bool uEyeCam::SetExposure(double exposureTime){
    INT returnVal;
    double param1 = exposureTime;
    returnVal = is_Exposure(my_hcamera, IS_EXPOSURE_CMD_SET_EXPOSURE, &param1, sizeof(param1));
    return (returnVal == IS_SUCCESS);
}

double uEyeCam::GetExposure(){
    INT returnVal;
    double param1;
    returnVal = is_Exposure(my_hcamera, IS_EXPOSURE_CMD_GET_EXPOSURE, &param1, sizeof(param1));
    if (returnVal == IS_SUCCESS) return param1;

    return NULL;
}

bool uEyeCam::SetGamma(INT gamma) {
    INT param1 = gamma;
    INT returnVal = is_Gamma(my_hcamera, IS_GAMMA_CMD_SET, &param1, sizeof(param1));
    return (returnVal == IS_SUCCESS);
}

INT uEyeCam::GetGamma(){
    INT param1;
    INT returnVal = is_Gamma(my_hcamera, IS_GAMMA_CMD_GET, &param1, sizeof(param1));
    if (returnVal == IS_SUCCESS) return param1;
    return NULL;
}

//Pixel Clock Values: 7 – 86
bool uEyeCam::SetPixelClock(UINT pixelClock){
    UINT param1 = pixelClock;
    INT returnVal = is_PixelClock(my_hcamera, IS_PIXELCLOCK_CMD_SET, &param1, sizeof(param1));
    return (returnVal == IS_SUCCESS);

}

UINT uEyeCam::GetPixelClock(){
    UINT param1;
    INT returnVal = is_PixelClock(my_hcamera, IS_PIXELCLOCK_CMD_GET, &param1, sizeof(param1));
    if (returnVal == IS_SUCCESS) return param1;
    return NULL;
}

double uEyeCam::SetFrameRate(double FPSTarget) {
    double realFPS;
    INT returnVal = is_SetFrameRate(my_hcamera, FPSTarget, &realFPS);
    if (returnVal == IS_SUCCESS) return realFPS;
    return NULL;
}

double uEyeCam::GetFrameRate(){
    double currentFPS;
    INT returnVal = is_GetFramesPerSecond(my_hcamera,&currentFPS);
    if (returnVal == IS_SUCCESS) return currentFPS;
    return NULL;

}


/**
 * Calls the ring buffer constructor, starts the camera, and starts a new thread for image+timestamp acquisition
 */
void uEyeCam::CamStream() {

    CamSeqBuild();

    is_CaptureVideo(my_hcamera, IS_DONT_WAIT);
    CamRunImageQueue();
}

/**
 * Builds the ring buffer for image streaming
 * @return success/failure
 */
bool uEyeCam::CamSeqBuild() {

    bool returnBool = false;
    INT returnVal;
    uint8_t i;

    //how many buffers?
    double frameTimeMin, frameTimeMax, frameTimeInterval;
    returnVal = is_GetFrameTimeRange(my_hcamera, &frameTimeMin, &frameTimeMax, &frameTimeInterval);
    if (returnVal == IS_SUCCESS){

        double maxBuffers;
        maxBuffers = (1.0f/frameTimeMin) + 0.5f;
        my_nNumberOfBuffers = (uint8_t) (maxBuffers);

        if (my_nNumberOfBuffers < 3)
            my_nNumberOfBuffers = 3;
    }

    else return false;

    INT iImgMemID = 0;
    char* pcImgMem = nullptr;

    for (i=0; i < my_nNumberOfBuffers; i++)
    {
        //allocate a single buffer memory
        returnVal = is_AllocImageMem(my_hcamera, my_nSizeX, my_nSizeY, my_BitsPerPixel, &pcImgMem, &iImgMemID);

        if (returnVal != IS_SUCCESS)
            break;

        returnVal = is_AddToSequence(my_hcamera, pcImgMem, iImgMemID);
        if (returnVal != IS_SUCCESS) {
            is_FreeImageMem(my_hcamera, pcImgMem, iImgMemID);
            break;
        }

        my_vpcSeqImgMem.push_back(pcImgMem);
        my_videoSeqMemId.push_back(iImgMemID);
    }

    my_nNumberOfBuffers = i;

    returnVal = is_InitImageQueue(my_hcamera, 0);
    if (returnVal == IS_SUCCESS && my_nNumberOfBuffers >= 3)
        returnBool = true;

    return returnBool;
}

/**
 * Runs the initialiser for the thread responsible for popping the next image from the ring buffer and saving it.
 * @return  success/failure
 */
bool uEyeCam::CamRunImageQueue(){
    bool returnBool = false;

    my_boRunThreadImageQueue = TRUE;

    pthread_attr_setstacksize (&attr, 0);
    pthread_create(&pthread1, &attr, threadProcImageQueueInit, (void*) this);
    if (&pthread1 == nullptr)
        printf("ERROR: Cannot create new image queue thread! \n");
    my_boRunThreadImageQueue = FALSE;
    pthread_join(pthread1, NULL);
    pthread_exit(&pthread1);



}

/**
 * Pops the next image off the queue and saves it as an image file. Also saves timestamp data to a csv.
 */
void uEyeCam::ThreadProcImageQueue() {
    INT nMemID = 0;
    char *pBuffer = nullptr;
    INT returnVal;
    UEYEIMAGEINFO imgInfo;
    uint64_t tStamp = 0;
    static uint32_t imgNo = 0;


    std::wofstream file;
    file.open("tstamps.csv");

    printf("opening file\n");
    printf("recording...\n");
    do
    {
        my_boTerminatedThreadImageQueue = false;
        returnVal = is_WaitForNextImage(my_hcamera, 10000, &pBuffer, &nMemID);
        if (returnVal == IS_SUCCESS)
        {
            is_GetImageInfo(my_hcamera, nMemID, &imgInfo, sizeof(imgInfo));
            tStamp = imgInfo.u64TimestampDevice;

            std::wstringstream ss;
            ss.width(7);
            ss.fill('0');
            ss  << ++imgNo <<".bmp";
            std::wstring WideString =  ss.str();
            wchar_t * WideStringCStr = new wchar_t [WideString.length() + 1];
            wcscpy(WideStringCStr, WideString.c_str());


            imageFileParams.pwchFileName = WideStringCStr;
            imageFileParams.nFileType = IS_IMG_BMP;
            imageFileParams.ppcImageMem = NULL;
            imageFileParams.pnImageID = NULL;

            is_ImageFile(my_hcamera, IS_IMAGE_FILE_CMD_SAVE, (void *) &imageFileParams, sizeof(imageFileParams));
            file << WideStringCStr << ',' << tStamp << std::endl;
            is_UnlockSeqBuf(my_hcamera, nMemID, pBuffer);
        }
    }
    while(&pthread1);

    file.close();
    my_boTerminatedThreadImageQueue = true;
}


/**
 * Snaps one image each time it is called, saving it to disk along with its timestamp.
 */
void uEyeCam::CamGrab() {

    INT returnVal;
    INT nMemID = 0;
    char *pcImgMem = nullptr;
    uint64_t tStamp = 0;
    static uint32_t imgNo = 0;


    is_FreezeVideo(my_hcamera, IS_DONT_WAIT);

    returnVal = is_GetActiveImageMem(my_hcamera, &pcImgMem, &nMemID);

    if (returnVal == IS_SUCCESS) {

        UEYEIMAGEINFO imgInfo;


        std::wofstream file;
        file.open("tstamps_snapshots.csv");

        is_GetImageInfo(my_hcamera, nMemID, &imgInfo, sizeof(imgInfo));
        tStamp = imgInfo.u64TimestampDevice;

        std::wstringstream ss;
        ss.width(7);
        ss.fill('0');
        ss << ++imgNo << "_SNAPSHOT.bmp";
        std::wstring WideString = ss.str();
        auto *WideStringCStr = new wchar_t[WideString.length() + 1];
        wcscpy(WideStringCStr, WideString.c_str());

        imageFileParams.pwchFileName = WideStringCStr;
        imageFileParams.nFileType = IS_IMG_BMP;
        imageFileParams.ppcImageMem = NULL;
        imageFileParams.pnImageID = NULL;

        is_ImageFile(my_hcamera, IS_IMAGE_FILE_CMD_SAVE, (void *) &imageFileParams, sizeof(imageFileParams));
        file << "SNAPSHOT " << WideStringCStr << ',' << tStamp << std::endl;

        file.close();
    }
}






/**
 * destroys the ring buffer for image streaming
 * @return success/failure
 */
bool uEyeCam::CamSeqKill() {
    is_ExitImageQueue(my_hcamera);
    is_ClearSequence(my_hcamera);

    for (int i=my_nNumberOfBuffers-1; i>=0; i--)
    {
        if (is_FreeImageMem(my_hcamera, my_vpcSeqImgMem.at(i), my_videoSeqMemId.at(i)) != IS_SUCCESS)
            return false;
    }

    my_videoSeqMemId.clear();
    my_vpcSeqImgMem.clear();
    my_nNumberOfBuffers = 0;

    return true;
}




/**
 * Initialises a thread which points to the camera-object and calls the thread processing image method
 * @param pv - Pointer to the camera object
 */
void * threadProcImageQueueInit(void * pv){
    uEyeCam * p = (uEyeCam *) pv;
    printf("Starting image proc thread!\n");
    p->ThreadProcImageQueue();

}


