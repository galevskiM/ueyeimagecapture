cmake_minimum_required(VERSION 3.5)
project(uEyeImageCaptureRT)


set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)
set(GCC_COMPILER_VERSION "7.3" CACHE STRING "GCC Compiler version")

#if (${CROSS_COMPILE})
#
#        set(CMAKE_SYSTEM_PROCESSOR arm)
#
#        set(FLOAT_ABI_SUFFIX "hf")
#        set(CMAKE_C_COMPILER arm-linux-gnueabi${FLOAT_ABI_SUFFIX}-gcc-${GCC_COMPILER_VERSION})
#        set(CMAKE_CXX_COMPILER arm-linux-gnueabi${FLOAT_ABI_SUFFIX}-g++-${GCC_COMPILER_VERSION})
#        set(ARM_LINUX_SYSROOT /usr/arm-linux-gnueabi${FLOAT_ABI_SUFFIX} CACHE PATH "ARM cross-compilation system root")
#        set(CMAKE_CXX_FLAGS                 ""          CACHE STRING "c++ flags")
#        set(CMAKE_C_FLAGS                   ""          CACHE STRING "c flags")
#        set(CMAKE_SHARED_LINKER_FLAGS       ""          CACHE STRING "shared linker flags")
#        set(CMAKE_MODULE_LINKER_FLAGS       ""          CACHE STRING "module linker flags")
#        set(CMAKE_EXE_LINKER_FLAGS          ""          CACHE STRING "executable linker flags")
#
#        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mthumb -fdata-sections -Wa,--noexecstack -fsigned-char -Wno-psabi")
#        set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -mthumb -fdata-sections -Wa,--noexecstack -fsigned-char -Wno-psabi")
#
#        set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--fix-cortex-a8 -Wl,--no-undefined -Wl,--gc-sections -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now ${CMAKE_SHARED_LINKER_FLAGS}")
#        set(CMAKE_MODULE_LINKER_FLAGS "-Wl,--fix-cortex-a8 -Wl,--no-undefined -Wl,--gc-sections -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now ${CMAKE_MODULE_LINKER_FLAGS}")
#        set(CMAKE_EXE_LINKER_FLAGS    "-Wl,--fix-cortex-a8 -Wl,--no-undefined -Wl,--gc-sections -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now ${CMAKE_EXE_LINKER_FLAGS}")
#
#        set(COMPILER_IS_ARM "1")
#        add_definitions(-D_ARM_TEGRA3)
#endif()

set(CMAKE_CXX_STANDARD 11)

find_package(OpenCV REQUIRED)
find_library(uEYE_API ueye_api)

INCLUDE_DIRECTORIES("${PROJECT_BINARY_DIR}")
INCLUDE_DIRECTORIES("${PROJECT_SOURCE_DIR}")

set(SOURCES imgaq_main.cpp uEyeCam.cpp uEye.h uEyeCam.h)
add_executable(uEyeImageCaptureRT ${SOURCES})


target_link_libraries(uEyeImageCaptureRT ${OpenCV_LIBS})
target_link_libraries(uEyeImageCaptureRT ${uEYE_API})
target_link_libraries(uEyeImageCaptureRT pthread)