#include "uEyeUSB3.h"
#include "uEye.h"
#include <stdio.h>
#include <pthread.h>
#include <string>
#include <sstream>
#include <cstring>
#include <iomanip>
#include <fstream>

pthread_t pthread1;
pthread_attr_t attr;

INT uEyeUSB3::InitCamera(HIDS * hcamera)
{
    INT returnVal = is_InitCamera(hcamera, nullptr);

    if (returnVal == IS_STARTER_FW_UPLOAD_NEEDED) {
        INT nUploadTime = 25;
        is_GetDuration(*hcamera, IS_STARTER_FW_UPLOAD, &nUploadTime);

        printf("The camera requires new firmware. The upload will take about %d seconds. Please wait...\n", nUploadTime);

        *hcamera = (HIDS) (((INT) *hcamera) | IS_ALLOW_STARTER_FW_UPLOAD);
        returnVal = is_InitCamera(hcamera, NULL);
    }

    return returnVal;
}
uEyeUSB3::~uEyeUSB3() {}


bool uEyeUSB3::Open(){

    INT returnVal;

    if (my_hcamera != 0 || my_hcamera != 1)
        returnVal = is_ExitCamera(my_hcamera);

    my_hcamera = (HIDS) 0;
    returnVal = InitCamera(&my_hcamera);

    if (returnVal == IS_SUCCESS) {
        is_GetSensorInfo(my_hcamera, &my_sInfo);

        returnVal = is_SetErrorReport(my_hcamera, IS_DISABLE_ERR_REP);
        if (returnVal != IS_SUCCESS)
        {
            printf("ERROR: Cannot disable automatic error reports \n");
            return false;
        }
        SENSORINFO SensorInfo;
        is_GetSensorInfo(my_hcamera, &SensorInfo);
        CAMINFO CamInfo;
        is_GetCameraInfo(my_hcamera, &CamInfo);

        //assuming colour camera:
        my_BitsPerPixel = 24;
        my_nColorMode = IS_CM_BGR8_PACKED;

        is_SetColorMode(my_hcamera, my_nColorMode);

        CamSeqBuild();

        is_CaptureVideo(my_hcamera, IS_DONT_WAIT);

    }

    else {
        printf("ERROR: Cannot open the uEye Camera!\n");
        return false;
    }
    return true;
}

bool uEyeUSB3::CamSeqBuild() {
    bool returnBool = false;
    INT returnVal;
    uint8_t i;

    //how many buffers?
    double frameTimeMin, frameTimeMax, frameTimeInterval;
    returnVal = is_GetFrameTimeRange(my_hcamera, &frameTimeMin, &frameTimeMax, &frameTimeInterval);
    if (returnVal == IS_SUCCESS)
    {
        double maxBuffers;
        maxBuffers = (1.0f/frameTimeMin) + 0.5f;
        my_nNumberOfBuffers = (uint8_t) (maxBuffers);

        if (my_nNumberOfBuffers < 3)
        {
            my_nNumberOfBuffers = 3;
        }

    }

    else
        return false;

    IS_SIZE_2D imageSize;
    is_AOI(my_hcamera, IS_AOI_IMAGE_GET_SIZE, (void*)&imageSize, sizeof(imageSize));
    INT nAllocSizeX = 0;
    INT nAllocSizeY = 0;
    my_nSizeX = nAllocSizeX = imageSize.s32Width;
    my_nSizeY = nAllocSizeY = imageSize.s32Height;
    UINT nAbsPosX = 0;
    UINT nAbsPosY = 0;
    is_AOI(my_hcamera, IS_AOI_IMAGE_GET_POS_X_ABS, (void*)&nAbsPosX, sizeof(nAbsPosX));
    is_AOI(my_hcamera, IS_AOI_IMAGE_GET_POS_Y_ABS, (void*)&nAbsPosY, sizeof(nAbsPosY));
    if (nAbsPosX)
        nAllocSizeX = my_sInfo.nMaxWidth;
    if (nAbsPosY)
        nAllocSizeY = my_sInfo.nMaxHeight;

    INT iImgMemID = 0;
    char* pcImgMem = nullptr;

    for (i=0; i < my_nNumberOfBuffers; i++)
    {


        //allocate a single buffer memory
        returnVal = is_AllocImageMem(my_hcamera, nAllocSizeX, nAllocSizeY, my_BitsPerPixel, &pcImgMem, &iImgMemID);
        if (returnVal != IS_SUCCESS)
            break;

        returnVal = is_AddToSequence(my_hcamera, pcImgMem, iImgMemID);
        if (returnVal != IS_SUCCESS) {
            is_FreeImageMem(my_hcamera, pcImgMem, iImgMemID);
            break;
        }

        my_vpcSeqImgMem.push_back(pcImgMem);
        my_videoSeqMemId.push_back(iImgMemID);
    }

    my_nNumberOfBuffers = i;

    returnVal = is_InitImageQueue(my_hcamera, 0);
    if (returnVal == IS_SUCCESS && my_nNumberOfBuffers >= 3)
            returnBool = true;

    return returnBool;
}

bool uEyeUSB3::CamSeqKill() {
    is_ExitImageQueue(my_hcamera);
    is_ClearSequence(my_hcamera);

    for (int i=my_nNumberOfBuffers-1; i>=0; i--)
    {
        if (is_FreeImageMem(my_hcamera, my_vpcSeqImgMem.at(i), my_videoSeqMemId.at(i)) != IS_SUCCESS)
            return false;
    }

    my_videoSeqMemId.clear();
    my_vpcSeqImgMem.clear();
    my_nNumberOfBuffers = 0;

    return true;
}

bool uEyeUSB3::GrabImagesFromQueue(){
    bool returnBool = false;

    my_boRunThreadImageQueue = TRUE;

    pthread_attr_setstacksize (&attr, 0);
    pthread_create(&pthread1, &attr, threadProcImageQueueInit, (void*) this);
    if (&pthread1 == nullptr)
        printf("ERROR: Cannot create new image queue thread! \n");
    my_boRunThreadImageQueue = FALSE;
    pthread_join(pthread1, NULL);
    pthread_exit(&pthread1);



}

void * threadProcImageQueueInit(void * pv){
    auto * p = (uEyeUSB3 *) pv;
    printf("Starting image proc thread!\n");
    p->ThreadProcImageQueue();

}

void uEyeUSB3::ThreadProcImageQueue() {
    INT nMemID = 0;
    char *pBuffer = nullptr;
    INT returnVal;
    UEYEIMAGEINFO imgInfo;
    uint64_t tStamp = 0;
    static uint32_t imgNo = 0;

    printf("\nClearing old timestamp data in directory...\n");
    if (remove("tstamps.csv") != 0)
        printf("Error clearing old timestamps.\n");
    else
        printf("Old timestamp data cleared!\n\n");
    std::wofstream file;
    file.open("tstamps.csv");
    printf("Creating new timestamp data file...\n");

    printf("Recording...\n");

    do
    {
        my_boTerminatedThreadImageQueue = false;
        returnVal = is_WaitForNextImage(my_hcamera, 10000, &pBuffer, &nMemID);
        if (returnVal == IS_SUCCESS)
        {
            is_GetImageInfo(my_hcamera, nMemID, &imgInfo, sizeof(imgInfo));
            tStamp = imgInfo.u64TimestampDevice;

            std::wstringstream ss;
            ss.width(7);
            ss.fill('0');
            ss  << ++imgNo << ".jpg";
            std::wstring WideString =  ss.str();
            auto * WideStringCStr = new wchar_t [WideString.length() + 1];
            wcscpy(WideStringCStr, WideString.c_str());


            imageFileParams.pwchFileName = WideStringCStr;
            imageFileParams.nFileType = IS_IMG_JPG;
            imageFileParams.nQuality = 80; // Quality ranges from 0 to 100, with 100 being the best. 75 is default.
            imageFileParams.ppcImageMem = NULL;
            imageFileParams.pnImageID = NULL;

            is_ImageFile(my_hcamera, IS_IMAGE_FILE_CMD_SAVE, (void *) &imageFileParams, sizeof(imageFileParams));
            file << WideStringCStr << ',' << tStamp << std::endl;
            is_UnlockSeqBuf(my_hcamera, nMemID, pBuffer);
        }
    }
    while(&pthread1);

    file.close();
    my_boTerminatedThreadImageQueue = true;
}

bool uEyeUSB3::Close(){
    bool returnBool = false;
    INT returnVal;
    returnBool = CamSeqKill();
    if (!returnBool) {
        printf("Error closing buffer!\n");
        return false;
    }

    returnVal = is_ExitCamera(my_hcamera);
    if (returnVal != IS_SUCCESS){
        printf("Error running camera exit function!\n");
        return false;
    }
    return true;
}
