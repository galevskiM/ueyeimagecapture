//
// Created by marko on 2/05/18.
//

#include "uEyeCam.h"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <fstream>

uEyeCam imgcapture;

int main() {

    imgcapture.CamOpen();
    imgcapture.CamStream();
    imgcapture.CamSeqKill();

    return 0;
}

